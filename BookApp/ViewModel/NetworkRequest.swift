//
//  NetworkRequest.swift
//  BookStandApp
//
//  Created by 大川葵 on 2019/01/09.
//  Copyright © 2019 Nexceed. All rights reserved.
//

import Foundation
import Moya
import SwiftyJSON
import RxSwift
import Alamofire

// MARK: NetworkRequest
final class NetworkRequest {
    
    //singleton
    public static let shared = NetworkRequest()
    //initializa
    private init() {}
    
    
    // MARK: Private
    
    private let provider = MoyaProvider<NetworkCommunication>()
    
    func sync(tag: String) -> Observable<BookData> {
        return Observable<BookData>.create({ observable -> Disposable in
            self.provider.request(NetworkCommunication.bookDataGet(tag: tag)) { result in
                switch result {
                case .success(let response):
                    let json = JSON(response.data)//データ型が良い
                    let bookdata = BookData.init(json: json, isbn: tag)
                    observable.onNext(bookdata)
                case .failure(let error):
                    observable.onError(error)
                }
            }
            return Disposables.create()
        })
    }
    
    func post(isbns: [String]) -> Observable<Void> {
        return Observable<Void>.create({ observable -> Disposable in
            self.provider.request(NetworkCommunication.isbnPost(isbns: isbns)) { result in
                switch result {
                case .success(_):
                    observable.onNext(())
                case .failure(let error):
                    observable.onError(error)
                }
            }
            return Disposables.create()
        })
    }
    
    
}
