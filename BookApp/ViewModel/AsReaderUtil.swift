//
//  AsReaderTagRead.swift
//  BookStandApp
//
//  Created by 大川葵 on 2019/01/07.
//  Copyright © 2019 Nexceed. All rights reserved.
//

import Foundation
import RxSwift


// MARK: AsReaderUtil

class AsReaderUtil: NSObject, AsReaderRFIDDeviceDelegate, AsReaderDeviceDelegate, AsreaderBarcodeDeviceDelegate, AsReaderDelegate {
    
    // Singleton
    static let shared = AsReaderUtil()
    // Initializa
    private override init() {}
    
    
    // MARK: Public
    
    var isComboType: Bool = true
    var workingMode: Int32 = ASREADER_DEVICE_BARCODE
    var readEvent = ReadEvent.shared
    var mDevice = AsReaderDevice()
    var state = AsReaderState.unplugged
    
    
    // MARK: GUNInstance
    
    var mAsReader: AsReader?
    var mAsReaderGUN: AsReaderGUN?
    var readerDelegate: AsReaderDelegate?//シングルトンクラスなのでここでも宣言可
    
    
    // MARK: AsReaderState
    
    enum AsReaderState {
        case unplugged
        case plugged
        case connected
        case reading
    }
    
    
    // MARk: Private
    
    private let readerStatusSubject = BehaviorSubject<AsReaderState>(value: AsReaderState.unplugged)
    var readerStatus: Observable<AsReaderState> { return readerStatusSubject }
    
    
    // MARK: AsReaderDeviceDelegate
    
    func plugged(_ plug: Bool) {
        if plug {
            self.pluggedState()
        } else {
            self.unpluggedState()
        }
    }
    
    func readerConnected(_ status: Int32) {
        DispatchQueue.main.async {
            if status == 0xff {
                self.connectedState()
                let barcode = AsReaderBarcodeDevice.sharedInstance()
                (barcode as! AsReaderBarcodeDevice).delegateBarcode = self
                (barcode as! AsReaderBarcodeDevice).delegateDevice = self
            } else {
                self.disConnectedState()
            }
        }
    }
    
    //setReaderPowerが正常に呼ばれたときにtrueが返ってくる
    func responsePower(onOff isOn: Bool, hwModeChange isHWModeChange: Bool) {
        print(isOn)
    }
    
    //コンボタイプのトリガーボタン用のデリゲート
    func releasedTriggerButton() {
        print("releasedTriggerButton")
    }
    
    //コンボタイプのトリガーボタン用のデリゲート
    func pushedTriggerButton() {
        print("pushedTriggerButton")
    }
    
    func on(asReaderTriggerKeyEventStatus status: String!) {
        print(status)
    }
    
    //スキャンされたデータを取得した時
    func receivedScanData(_ readData: Data!) {
        var tag: String //取得したデータをString型にして格納する
        //取得したデータに応じてString型への変換方法が異なるので、場合分け
        switch (AsReaderInfo.sharedInstance() as AnyObject).currentSelectDevice {
        case ASREADER_DEVICE_BARCODE:
            tag = String(data: readData, encoding: String.Encoding.shiftJIS)!
        case ASREADER_DEVICE_RFID:
            tag = readData.hexadecimalString()!
        default:
            tag = "illegal Protocols"
        }
        readEvent.tagEventSubject.onNext(tag)
    }
    
    func unknownCommandReceived(_ commandCode: Int32) {
        print("unknownCommandReceived")
    }
    
    func allDataReceived(_ data: Data!) {
        print(data)
    }
    
    func batteryReceived(_ battery: Int32) {
        print("battery: \(battery)%")
    }
    
    func stopReadScan(_ status: Int32) {
        print("stopReadScan")
    }
    
    func startedReadScan(_ status: Int32) {
        print("startefReadScan")
    }
    
    func errorReceived(_ errorCode: Data!) {
        print(errorCode)
    }
    
    
    // MARK: AsReaderCombo function
    
    func pluggedState() {
        let asReaderInfo = AsReaderInfo.sharedInstance()
        
        if (asReaderInfo! as AnyObject).isPowerOn {
            self.connectedState()
        } else {
            self.state = AsReaderState.plugged
            readerStatusSubject.onNext(AsReaderState.plugged)
            print(state)
        }
    }
    
    func unpluggedState() {
        self.disConnectedState()
        self.state = AsReaderState.unplugged
        readerStatusSubject.onNext(AsReaderState.unplugged)
    }
    
    func connectedState() {
        self.state = AsReaderState.connected
        readerStatusSubject.onNext(AsReaderState.connected)
    }
    
    func disConnectedState() {
        self.state = AsReaderState.plugged
        readerStatusSubject.onNext(AsReaderState.plugged)
    }
    
    func connect(with mode: Int32) {
        workingMode = mode
        if isComboType {
            if state == AsReaderState.plugged {
                //電源が入ってなかった時
                self.mDevice.setReaderPower(true, beep: true, vibration: false, led: true, illumination: true, mode: mode)//Comboタイプのとき
            } else if state == AsReaderState.connected {
                //電源が入ってたとき
                self.mDevice.setReaderPower(false, beep: true, vibration: false, led: true, illumination: true, mode: mode)
            }
        }
    }
    
    
    // MARK: AsReaderGun function
    //GUNが接続されたときの処理　ObjectiveCで書かれている関数をSwiftで呼ぶ時に@objcをつけている
    @objc func AsReaderGUNConnected(notification: Notification?) {
        self.isComboType = false
        DispatchQueue.main.async {
            self.state = AsReaderState.connected
            self.readerStatusSubject.onNext(AsReaderState.connected)
            self.mAsReader = AsReader.init(asReaderGUN: self.mAsReaderGUN, delegate: self.readerDelegate)
            self.mAsReader?.setDelegate(self)
            if self.workingMode == ASREADER_DEVICE_RFID {
                self.mAsReader?.setBarcodeMode(false, isKeyAction: true)
                self.mAsReader?.setScanMode(RFIDScanMode)
            } else {
                self.mAsReader?.setBarcodeMode(true, isKeyAction: true)
                self.mAsReader?.setScanMode(BarcodeScanMode)
            }
        }
        print(mAsReader?.getScanMode() == RFIDScanMode ? "RFID" : "Barcode")
    }
    
    @objc func AsReaderGUNDisConnected(notification: Notification?) {
        self.isComboType = true
        DispatchQueue.main.async {
            self.state = AsReaderState.unplugged
            self.readerStatusSubject.onNext(AsReaderState.unplugged)
            self.mAsReader = nil
            self.mAsReader?.setDelegate(nil)
        }
    }
    
    
     // MARK: AsReader Gun delegate
    //以下3つのデリゲートはAsReaderDelegateのオプションの関数だが実装が必要
    func on(asReaderTriggerKeyEvent status: Bool) -> Bool {
        return true
    }
    
    //GUNタイプで1次元バーコードを読んだとき
    func detect(_ barcodeType: BarcodeType, codeId: String!, barcode: String!) {
        readEvent.tagEventSubject.onNext(barcode)
    }
    
    //GUNタイプでRFID読んだとき
    func readTag(_ tag: String!, rssi: Float, phase: Float, frequency: Float) {
       readEvent.tagEventSubject.onNext(tag)
    }
    
    
    // MARK: prepare
    
    func prepare() {
        //Combo Type
        AsReaderDevice.setTriggerModeDefault(true)
        
        ///Gun Type
        mAsReaderGUN = AsReaderGUN.init(deviceModel: "com.asreader.gun")
        mAsReaderGUN?.deviceModel = "com.asreader.gun"
        
        //Gunタイプが接続された時に通知を受け取る
        NotificationCenter.default.addObserver(self, selector: #selector(self.AsReaderGUNConnected), name: Notification.Name("AsReaderGUNConnected"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.AsReaderGUNDisConnected), name: Notification.Name("AsReaderGUNDisConnected"), object: nil)
        
        self.mDevice.delegateDevice = self
        UIDevice.current.isBatteryMonitoringEnabled = true
        
    }
    
    
    // MARK: reset
    //アプリを落とした時に呼ばないと、Observerが残ったままになり次回の時にうまく起動できない
    func reset() {
        self.mDevice.delegateDevice = nil
        UIDevice.current.isBatteryMonitoringEnabled = false
    
        NotificationCenter.default.removeObserver(self, name: Notification.Name("AsReaderGUNConnected"), object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name("AsReaderGUNDisConnected"), object: nil)
        
        mAsReader?.setDelegate(self)
        mAsReader?.stopSync()
    }
    
    

    
    
    
}
