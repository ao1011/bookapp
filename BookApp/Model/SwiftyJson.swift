//
//  SwiftyJson.swift
//  BookApp
//
//  Created by 大川葵 on 2019/01/11.
//  Copyright © 2019 Nexceed. All rights reserved.
//

import Foundation
import Moya
import SwiftyJSON

class BookData {
    var title: String?
    var author: String?
    var publisher: String?
    var image: String?
    var isbn: String
    var count: Int
    
    required init(json: JSON, isbn: String) {
        self.title = json["Items"][0]["Item"]["title"].stringValue
        self.author = json["Items"][0]["Item"]["author"].stringValue
        self.publisher = json["Items"][0]["Item"]["publisherName"].stringValue
        self.image = json["Items"][0]["Item"]["smallImageUrl"].stringValue
        self.isbn = isbn
        self.count = json["count"].intValue
    }
}

//rawString    String    "{\"error\":\"wrong_parameter\",\"error_description\":\"BooksBook/Search doesn\'t have version 20170706?format=json&amp;applicationId=1082019598952018143&amp;isbn=9784774142043\"}"    
