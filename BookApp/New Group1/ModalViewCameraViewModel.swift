//
//  ModalViewCameraViewModel.swift
//  BookApp
//
//  Created by 大川葵 on 2019/01/21.
//  Copyright © 2019 Nexceed. All rights reserved.
//

import Foundation
import RxSwift
import AVFoundation

// MARK: - ModalViewCameraVeiwModel
final class ModalViewCameraViewModel: NSObject, AVCaptureMetadataOutputObjectsDelegate {
    
    
    // MARK: AVCaptureMetadataOutputObjectsDelegate
    
    // メタデータの識別関数　Swift4から変更あり
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObject: [AVMetadataObject], from connection: AVCaptureConnection) {
        
        let objects = metadataObject
        let barcodeTypes = [AVMetadataObject.ObjectType.ean8, AVMetadataObject.ObjectType.ean13]
        //metadataの方がEAN-8もしくはEAN-13であるかチェック
        for metadataObject in objects {
            loop: for type in barcodeTypes {
                guard metadataObject.type == type else { continue }
                guard self.capturePreviewLayer.transformedMetadataObject(for: metadataObject) is AVMetadataMachineReadableCodeObject else { continue }
                if let object = metadataObject as? AVMetadataMachineReadableCodeObject {
                    // readableコードならbreak
                    let detectionString = object.stringValue
                    readEvent.tagEventSubject.onNext(detectionString!)
                    break loop
                }
                
            }
        }
    }
    
    
    // MARK: Private
    
    private weak var viewController: ModalViewCameraController?
    
    private let disposeBag = DisposeBag()
    private let readEvent = ReadEvent.shared
     //カメラやマイクの入出力を管理するオブジェクトを生成
    private lazy var captureSession: AVCaptureSession = AVCaptureSession()
    //キャプチャデバイスの設定
    private lazy var captureDevice: AVCaptureDevice = AVCaptureDevice.default(for: AVMediaType.video)!
    //カメラの映像を画面に表示するためのレイヤーを設定
    private lazy var capturePreviewLayer: AVCaptureVideoPreviewLayer = {
        let layer = AVCaptureVideoPreviewLayer(session: self.captureSession)
        return layer
    }()
    private var captureInput: AVCaptureInput? = nil
    private var test: String? = nil
    private lazy var captureOutput: AVCaptureMetadataOutput = {
        let output = AVCaptureMetadataOutput()
        output.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        return output
    }()
    
    
    // MARK: Internal
    
    init(_ viewController: ModalViewCameraController) {
        
        self.viewController = viewController
    }
    
    func setUpBarcodeCapture(captureView: UIView!) {
        do {
            captureInput = try AVCaptureDeviceInput(device: captureDevice)
            captureSession.addInput(captureInput!)
            captureSession.addOutput(captureOutput)
            
            captureOutput.metadataObjectTypes = captureOutput.availableMetadataObjectTypes
            capturePreviewLayer.frame = captureView.bounds ?? CGRect.zero
            capturePreviewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
            captureView.layer.addSublayer(capturePreviewLayer)
        } catch let error as NSError {
            print(error)
        }
    }
    
    func startSet(captureView: UIView!) {
        capturePreviewLayer.frame = captureView?.bounds ?? CGRect.zero
        captureSession.startRunning()
    }
    
}
