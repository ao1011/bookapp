//
//  Test.swift
//  BookApp
//
//  Created by 大川葵 on 2019/01/17.
//  Copyright © 2019 Nexceed. All rights reserved.
//

import Foundation
import RxSwift

// MARK: ObservableType

extension ObservableType where E: Equatable {
    func stateBuffer() -> Observable<E> {
        var currentState: E?
        var result = false
        return self.do(onNext: { value in
            if currentState == nil || currentState! != value {
                result = true
                currentState = value
            }
        }).flatMap{ value -> Observable<E> in
            defer { result = false }
            if result {
                return Observable<E>.just(value)
            } else {
                return Observable<E>.empty()
            }
        }
    }
}


// MARK: ObservableType

extension ObservableType {
    func stateBuffer(_ comparer: @escaping (E, E) -> Bool) -> Observable<E> {
        var currentState: E?
        var result = false
        return self.do(onNext: { value in
            if currentState == nil || !comparer(currentState!, value) {
                result = true
            }
            currentState = value
        }).flatMap{ value -> Observable<E> in
            defer { result = false }
            if result {
                return Observable<E>.just(value)
            } else {
                return Observable<E>.empty()
            }
        }
    }
}
