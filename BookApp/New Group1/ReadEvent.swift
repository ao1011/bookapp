//
//  ReadData.swift
//  BookApp
//
//  Created by 大川葵 on 2019/01/17.
//  Copyright © 2019 Nexceed. All rights reserved.
//

import Foundation
import RxSwift

// MARK: ReadEvent
class ReadEvent {
    
    
    // MARK: Internal
    
    static let shared = ReadEvent()
    private init(){}
    
    
    // MARK: Public
    //このObservableにすべてのtagイベントを流せる
    var readBook: [BookData]?
    var tagEventSubject = PublishSubject<String>()
    var tagEvent: Observable<String>? {return tagEventSubject}
    
}
