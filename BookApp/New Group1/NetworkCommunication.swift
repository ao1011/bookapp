//
//  NetworkCommunication.swift
//  BookApp
//
//  Created by 大川葵 on 2019/01/11.
//  Copyright © 2019 Nexceed. All rights reserved.
//

import Foundation
import Moya

// MARK: NetworkCommnication
enum NetworkCommunication {
    case bookDataGet(tag: String)
    case isbnPost(isbns: [String])
}

extension NetworkCommunication: TargetType {
    // MARK: Internal
    var baseURL: URL {
        switch self {
        case .bookDataGet:
            return URL(string: "https://app.rakuten.co.jp")!
        case .isbnPost:
            return URL(string: " ")!
        }
    }
    
    var path: String {
        switch self {
        case .bookDataGet(_):
            return "/services/api/BooksBook/Search/20170404"
        case .isbnPost:
            return " "
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .bookDataGet:
            return .get
        case .isbnPost:
            return .post
        }
    }
    
    var sampleData: Data {
        switch self {
        default:
            return Data()
        }
    }
    
    var task: Task {
        switch self {
        case .bookDataGet(let tag):
            return .requestParameters(parameters: ["format": "json", "applicationId": "1082019598952018143", "isbn": tag], encoding: URLEncoding.default)
        case .isbnPost(let isbns):
            return .requestParameters(parameters: [" ": isbns], encoding: JSONEncoding.default)
        }
    }
    
    var headers: [String : String]? {
        return ["Content-Type": "application/json"]
    }
    
    
}
