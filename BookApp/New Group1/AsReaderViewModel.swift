//
//  AsReaderViewModel.swift
//  BookApp
//
//  Created by Masahiro Watanabe on 2019/01/17.
//  Copyright © 2019 Nexceed. All rights reserved.
//

import RxCocoa
import RxSwift

// MARK: - AsReaderViewModel
final class AsReaderViewModel {
    
    
    // MARK: Private
    
    private weak var viewController: AsReaderViewController?
    
    private let reader = AsReaderUtil.shared
    private let request = NetworkRequest.shared
    private let disposeBag = DisposeBag()
    
    // 読み込んだバーコードがISBNコードとして利用可能かの判定
    private func convertISBN(value: String) -> String? {
        let v = NSString(string: value).longLongValue
        let prefix: Int64 = Int64(v / 10000000000)
        // ISBN-13の規格で、先頭３桁が978 のはず、でなければnilを返す
        guard prefix == 978 || prefix == 979 else {
            return nil
        }
        return value
    }
    
    
    // MARK: Internal
    
    var readBooks: [BookData] = []
    private(set) var isConnectButtonEnable = BehaviorRelay<Bool>(value: false)
    private(set) var connectButtonTitle = BehaviorRelay<String>(value: "Connect")
    private(set) var isSendButtonEnable = BehaviorRelay<Bool>(value: false)
    private(set) var lastReadingTag = PublishRelay<String>()
    private(set) var showAlert = PublishRelay<(String, String)>()
    private(set) var clearToggle = PublishRelay<Void>()
    
    init(_ viewController: AsReaderViewController) {
        
        self.viewController = viewController
        reader.prepare()
        let readerEvent = reader.readerStatus
        readerEvent.subscribe { [weak self] state in
            
            switch state.element! {
                
            case .unplugged:
                self?.isConnectButtonEnable.accept(false)
                self?.connectButtonTitle.accept("Connect")
            case .plugged:
                self?.isConnectButtonEnable.accept(true)
                self?.connectButtonTitle.accept("Connect")
            case .connected:
                self?.isConnectButtonEnable.accept(true)
                self?.connectButtonTitle.accept("Disconnect")
            case .reading:
                break
            }
        }
        .disposed(by: disposeBag)
        
        let tagEvent = ReadEvent.shared.tagEvent!
        
        tagEvent.stateBuffer()
            .share(replay: 1, scope: .whileConnected)
            .subscribe { [weak self] tag in
            guard let isbn = self?.convertISBN(value: tag.element!) else {
                
                return
            }
            
            self?.request.sync(tag: isbn)
                .subscribe(onNext: { [weak self] bookData in
                    let count = self?.readBooks.filter{$0.isbn == bookData.isbn}.count
                    if count == 0 {
                        
                        if bookData.count == 0 {
                            
                            self?.showAlert.accept(("データがありません", "楽天ブックスに書籍データが見つかりませんでした"))
                        } else {
                            
                            self?.readBooks.append(bookData)
                            self?.lastReadingTag.accept(isbn)
                            self?.isSendButtonEnable.accept(true)
                        }
                    }
                })
                .disposed(by: (self?.disposeBag)!)
        }
        .disposed(by: disposeBag)
    }
    
    deinit {
        
        reader.reset()
    }
    
    func tapConnectButton() {
        
        reader.connect(with: ASREADER_DEVICE_BARCODE)
    }
    
    func tapSendButton() {
        
        request.post(isbns: self.readBooks.compactMap{$0.isbn})
            .subscribe { [weak self] event in
                
                switch event {
                    
                case .next(_):
                    self?.showAlert.accept(("送信完了", "書籍データがストックされました"))
                case .error(_):
                    self?.showAlert.accept(("送信失敗", "書籍データが上手く送られませんでした"))
                case .completed:
                    break
                }
        }
        .disposed(by: disposeBag)
    }
    
    func clearButton() {
        self.readBooks.removeAll()
        clearToggle.accept(())
    }
}
