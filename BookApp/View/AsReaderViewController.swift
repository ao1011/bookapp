//
//  File.swift
//  BookStandApp
//
//  Created by 大川葵 on 2019/01/09.
//  Copyright © 2019 Nexceed. All rights reserved.
//

import UIKit

import RxSwift
import RxCocoa

// MARK: - AsReaderViewController
final class AsReaderViewController: UIViewController {
    
    // MARK: UIViewController
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var connectButton: UIBarButtonItem!
    @IBOutlet weak var sendButton: UIBarButtonItem!
    @IBOutlet weak var clearButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        viewModel = AsReaderViewModel(self)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = 120.0
        tableView.estimatedRowHeight = 120.0
        
        viewModel!.isConnectButtonEnable.asDriver()
        .drive(connectButton.rx.isEnabled)
        .disposed(by: disposeBag)
        
        viewModel!.connectButtonTitle.asDriver()
        .drive(connectButton.rx.title)
        .disposed(by: disposeBag)
        
        viewModel!.isSendButtonEnable.asDriver()
        .drive(sendButton.rx.isEnabled)
        .disposed(by: disposeBag)
        
        connectButton.rx.tap.asObservable()
            .subscribe(onNext: {[weak self] _ in
                
                    self?.viewModel!.tapConnectButton()
            })
        .disposed(by: disposeBag)
        
        sendButton.rx.tap
            .subscribe { [weak self] _ in
                
                self?.viewModel?.tapConnectButton()
        }
        .disposed(by: disposeBag)
        
        clearButton.rx.tap.asObservable()
            .subscribe(onNext: { [weak self] _ in
                self?.viewModel?.clearButton()
            })
        .disposed(by: disposeBag)
        
        viewModel!.lastReadingTag.asSignal()
            .emit(onNext: { [weak self] tag in
                
                self?.tableView.reloadData()
            })
        .disposed(by: disposeBag)
        
        viewModel!.showAlert.asSignal()
            .emit(onNext: { [weak self] (title, message) in
                
                let alertConeroller = UIAlertController(title: title,
                                                        message: message,
                                                        preferredStyle: UIAlertController.Style.alert)
                alertConeroller.addAction(UIAlertAction(title: "OK",
                                                        style: UIAlertAction.Style.default,
                                                        handler: nil))
                self?.present(alertConeroller, animated: true, completion: nil)
            })
        .disposed(by: disposeBag)
        
        viewModel!.clearToggle.asSignal()
            .emit(onNext: { [weak self] _ in
                self?.tableView.reloadData()
            })
        .disposed(by: disposeBag)
    }
    
    
    // MARK: Private
    
    private var viewModel: AsReaderViewModel?
    private let disposeBag = DisposeBag()
}

extension AsReaderViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return viewModel!.readBooks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: CustomCell = tableView.dequeueReusableCell(withIdentifier: "BooKDataCell", for: indexPath) as! CustomCell
        if viewModel!.readBooks.count == 0 {
            
            return cell
        } else {
            let count = viewModel!.readBooks.count
            cell.setCell(book: viewModel!.readBooks[ count - 1 - indexPath.row])
            return cell
        }
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let normalAction = UIContextualAction(style: .normal, title: "戻る") {
            (action, view, completionHandler) in completionHandler(true)
        }
        let deleteAction = UIContextualAction(style: .destructive, title: "削除") { (action, view, completionHandler) in
            completionHandler(true)
            self.viewModel?.readBooks.remove(at: indexPath.row)
        }
        let configration = UISwipeActionsConfiguration(actions: [normalAction, deleteAction])
        return configration
    }
}
