//
//  CustomCell.swift
//  BookApp
//
//  Created by 大川葵 on 2019/01/12.
//  Copyright © 2019 Nexceed. All rights reserved.
//

import UIKit
import SDWebImage
import Foundation

class CustomCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var publisherLabel: UILabel!
    @IBOutlet weak var bookImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //初期化
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setCell(book: BookData) {
        self.titleLabel.text = book.title
        self.authorLabel.text = book.author
        self.publisherLabel.text = book.publisher
        if let imageURL = book.image {
            bookImage.sd_setImage(with: URL(string: imageURL)) { (image, error, cacheType, url) in
                if error != nil {
                    print(error!)
                }
            }
        } else {
            bookImage.image = nil
        }
    }
    
}




