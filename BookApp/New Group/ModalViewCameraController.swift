//
//  ModalViewCameraController.swift
//  BookApp
//
//  Created by 大川葵 on 2019/01/22.
//  Copyright © 2019 Nexceed. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

// MARK: - ModalViewCameraController
class ModalViewCameraController: UIViewController {
    
    
    // MARK: Private
    
    private var viewModel: ModalViewCameraViewModel?
    private let disposeBag = DisposeBag()
    
    
    // MARK: UIViewController
    
    @IBOutlet weak var captureView: UIView!
    @IBOutlet weak var backButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel = ModalViewCameraViewModel(self)
        viewModel?.setUpBarcodeCapture(captureView: captureView)
        self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        viewModel?.startSet(captureView: captureView)
        
        backButton.rx.tap.asObservable()
            .subscribe(onNext: { _ in
                self.dismiss(animated: true, completion: nil)
            }).disposed(by: disposeBag)
                
    }
    
}
