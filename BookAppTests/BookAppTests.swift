//
//  BookAppTests.swift
//  BookAppTests
//
//  Created by 大川葵 on 2019/01/10.
//  Copyright © 2019 Nexceed. All rights reserved.
//

import XCTest
import RxSwift
@testable import BookApp

class BookAppTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        let testAsync: XCTestExpectation? = self.expectation(description: "AsReader-test")
        let reader = AsReaderUtil.shared
        let readerEvent = reader.readerStatus
        let tagEvent = reader.readTagEvent
        
        let disposeBag = DisposeBag()
        reader.prepare()
        
        readerEvent.subscribe { state in
            print(state)
            if state.element == AsReaderUtil.AsReaderState.plugged {
                reader.connect(with: ASREADER_DEVICE_BARCODE)
            }
            
            }.disposed(by: disposeBag)
        tagEvent.subscribe { tag in
            print(tag)
        }.disposed(by: disposeBag)
        reader.reset()
        self.waitForExpectations(timeout: 60, handler: nil)
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
