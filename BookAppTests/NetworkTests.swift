//
//  NetworkTests.swift
//  BookAppTests
//
//  Created by 大川葵 on 2019/01/11.
//  Copyright © 2019 Nexceed. All rights reserved.
//

import Foundation
import XCTest
import RxSwift
@testable import BookApp

class NetworkTests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testExample() {
        let testAsync: XCTestExpectation? = self.expectation(description: "AsReader-test")
        let request = NetworkRequest.shared
        let disposeBag = DisposeBag()
        
        request.sync(tag: "9784774142043").debug()
            .subscribe(onNext: { bookData in
                print(bookData.author)
                testAsync?.fulfill()
                }, onError: { error in
                    print(error)
            }).disposed(by: disposeBag)
//        testAsync?.fulfill()
        self.waitForExpectations(timeout: 60, handler: nil)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
}
